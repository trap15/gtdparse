OBJECTS   = gtdparse.o
OUTPUT    = gtdparse
TARGETS   = $(OUTPUT)
LIBS      = -lgd
CFLAGS    = -O3 $(INCLUDE)
LDFLAGS   = $(LIBS) -g
CLEANED   = $(OBJECTS) $(TARGETS)

.PHONY: all clean

all: $(OBJECTS) $(TARGETS)
%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(OUTPUT): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@
clean:
	$(RM) $(CLEANED)

