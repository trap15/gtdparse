/*
	GTDparse -- GTD file handler
	GTD->PNG decoder

Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <gd.h>

typedef struct {
	uint16_t unk1;
	uint16_t unk2;
	uint16_t x;
	uint16_t y;
	uint16_t paloff; /* From 0 */
	uint16_t palsize; /* In bytes??? */
	uint16_t bitmapsize; /* In bytes */
	uint16_t unk8;

	uint16_t* pal; /* RGB555 */
	uint8_t* bmp; /* 8bit, indexed from the palette */
} Gtd;

#define es16(x) (	(((x) & 0x00FF) << 8) | \
			(((x) & 0xFF00) >> 8))

#define UNRGB555(x) (	(((((x) >> 10) & 0x1F) * 0xFF / 0x1F) << 16) | \
			(((((x) >>  5) & 0x1F) * 0xFF / 0x1F) <<  8) | \
			(((((x) >>  0) & 0x1F) * 0xFF / 0x1F) <<  0))

int write_gtd_png(char *out, Gtd gtd, uint32_t* newpal)
{
	FILE* fp;
	gdImagePtr im;
	int x, y;
	fp = fopen(out, "wb");
	if(fp == NULL) {
		return 0;
	}
	im = gdImageCreateTrueColor(gtd.x, gtd.y);
	if(im == NULL) {
		fclose(fp);
		return 0;
	}
	gdImageAlphaBlending(im, 0);
	gdImageSaveAlpha(im, 1);
	for(y = 0; y < gtd.y; y++) {
		for(x = 0; x < gtd.x; x++) {
			/* Color index 0 is transparent */
			if(gtd.bmp[x + (y * gtd.x)] == 0) {
				int clr = gdTrueColorAlpha(0, 0, 0, 0x7F);
				gdImageSetPixel(im, x, y, clr);
				continue;
			}
			uint32_t rgba = newpal[gtd.bmp[x + (y * gtd.x)]];
			uint8_t r = (rgba >> 0)  & 0xFF;
			uint8_t g = (rgba >> 8) & 0xFF;
			uint8_t b = (rgba >> 16) & 0xFF;
			uint8_t a = 0;
			int clr = gdTrueColorAlpha(r, g, b, a);
			gdImageSetPixel(im, x, y, clr);
		}
	}
	gdImagePng(im, fp);
	fclose(fp);
	return 1;
}

void usage(char *app)
{
	fprintf(stderr, "Usage:\n"
		"	%s [in.gtd] [out.png]\n",
		app);
}

int main(int argc, char *argv[])
{
	FILE* fp;
	int size;
	uint8_t* data;
	Gtd gtd;
	uint32_t* newpal;
	int i;
	
	if(argc < 3) {
		usage(argv[0]);
		return EXIT_FAILURE;
	}
	
	fp = fopen(argv[1], "rb");
	if(fp == NULL) {
		perror("Unable to open input file");
		return EXIT_FAILURE;
	}
	if(fseek(fp, 0, SEEK_END) != 0) {
		perror("Unable to seek");
		fclose(fp);
		return EXIT_FAILURE;
	}
	size = ftell(fp);
	if(fseek(fp, 0, SEEK_SET) != 0) {
		perror("Unable to seek");
		fclose(fp);
		return EXIT_FAILURE;
	}
	data = malloc(size);
	if(data == NULL) {
		perror("Unable to allocate");
		fclose(fp);
		return EXIT_FAILURE;
	}
	if(fread(data, size, 1, fp) != 1) {
		perror("Unable to read file");
		fclose(fp);
		return EXIT_FAILURE;
	}
	fclose(fp);
	gtd.unk1 = es16(ntohs(*(uint16_t*)(data + 0x00)));
	gtd.unk2 = es16(ntohs(*(uint16_t*)(data + 0x02)));
	gtd.x = es16(ntohs(*(uint16_t*)(data + 0x04)));
	gtd.y = es16(ntohs(*(uint16_t*)(data + 0x06)));
	gtd.paloff = es16(ntohs(*(uint16_t*)(data + 0x08)));
	gtd.palsize = es16(ntohs(*(uint16_t*)(data + 0x0A)));
	gtd.bitmapsize = es16(ntohs(*(uint16_t*)(data + 0x0C)));
	gtd.unk8 = es16(ntohs(*(uint16_t*)(data + 0x0E)));
	printf( "Unk1    : %04X\n"
		"Unk2    : %04X\n"
		"X       : %04X\n"
		"Y       : %04X\n"
		"PalOff  : %04X\n"
		"PalSz   : %04X\n"
		"BmpSz   : %04X\n"
		"Unk8    : %04X\n",
		gtd.unk1, gtd.unk2, gtd.x, gtd.y,
		gtd.paloff, gtd.palsize, gtd.bitmapsize, gtd.unk8);
	
	
	gtd.pal = malloc(gtd.palsize);
	if(gtd.pal == NULL) {
		perror("Unable to allocate palette");
		return EXIT_FAILURE;
	}
	newpal = malloc(gtd.palsize * 2);
	if(newpal == NULL) {
		perror("Unable to allocate new palette");
		free(gtd.pal);
		return EXIT_FAILURE;
	}
	memcpy(gtd.pal, data + gtd.paloff, gtd.palsize);
	gtd.bmp = malloc(gtd.bitmapsize);
	if(gtd.bmp == NULL) {
		perror("Unable to allocate bitmap");
		free(gtd.pal);
		free(newpal);
		return EXIT_FAILURE;
	}
	memcpy(gtd.bmp, data + gtd.paloff + gtd.palsize, gtd.bitmapsize);
	free(data);
	
	for(i = 0; i < gtd.palsize/2; i++) {
		newpal[i] = UNRGB555(gtd.pal[i]);
	}
	free(gtd.pal);
	if(!write_gtd_png(argv[2], gtd, newpal)) {
		perror("Unable to write PNG");
		free(gtd.bmp);
		return EXIT_FAILURE;
	}
	free(gtd.bmp);
	return EXIT_SUCCESS;
}

